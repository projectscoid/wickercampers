#import "AppDelegate.h"
#import "Main+Controller.h"
@implementation AppDelegate {
@private
    Main_Controller *main_controller;
}
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [self setInitial];
    [self.window makeKeyAndVisible];
    return YES;
}
- (void)openMainController {
    main_controller = [[Main_Controller alloc] initWithNibName:@"Main+Controller" bundle:nil];
    self.window.rootViewController = main_controller;
}
- (void)setInitial {
    [self openMainController];
}
@end