#import <UIKit/UIKit.h>
@interface Main_Controller : UIViewController <UIWebViewDelegate>
@property (nonatomic, strong) IBOutlet UIWebView *web_view;
@property (nonatomic, strong) IBOutlet UIButton *btn_back;
@property (nonatomic, strong) IBOutlet UIButton *btn_forward;
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *pgb_loading;
@end