#import "Main+Controller.h"
@implementation Main_Controller {
@private
    UITapGestureRecognizer *tap_recognizer;
    bool _error;
}
- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [self.pgb_loading setHidden:true];
    [self.btn_back setEnabled:[webView canGoBack]];
    [self.btn_forward setEnabled:[webView canGoForward]];
    [self.btn_back setHidden:false];
    [self.btn_forward setHidden:false];
}
- (void)webViewDidStartLoad:(UIWebView *)webView {
    [self.pgb_loading setHidden:false];
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    _error = true;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setInitial];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (IBAction)doBack:(id)sender {
    [self.web_view goBack];
}
- (IBAction)doForward:(id)sender {
    [self.web_view goForward];
}
- (void)setInitial {
    tap_recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapAnywhere:)];
    [self.view addGestureRecognizer:tap_recognizer];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL: [NSURL URLWithString: @"http://wickedcampers.com.au/dev-mob/"] cachePolicy: NSURLRequestUseProtocolCachePolicy timeoutInterval:120];
    [self.web_view loadRequest:request];
}
- (void)didTapAnywhere:(UITapGestureRecognizer *)recognizer {
    if (_error) {
        _error = false;
        [self.web_view reload];
    }
}
@end